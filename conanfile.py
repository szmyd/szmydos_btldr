#!/usr/bin/env python
# -*- coding: utf-8 -*-
from conans import ConanFile, CMake

class SzmydosbtldrConan(ConanFile):
    name = "szmydos_btldr"
    version = "0.0.0"

    license = "MIT"
    url = "https://gitlab.com/szmyd/szmydos_btldr.git"
    description = "SzmydOS Bootloader"

    settings = "arch", "os", "compiler", "build_type"
    options = {"shared": ['True', 'False'],
               "fPIC": ['True', 'False']}
    default_options = 'shared=False', 'fPIC=True'

    generators = "cmake"
    exports_sources = "src/*"
    exports = "LICENSE.md"

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="src")
        cmake.build()

    def package(self):
        self.copy("btldr.bin", dst="obj")
